import {  Router, Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetails } from './employees/empdetails.component';
import { AddEditEmployee } from './employees/addeditemployee.component';

const routes: Routes = [
  { path: 'employees', component: EmployeesComponent },
  { path: 'details', component: EmployeeDetails },
  { path: 'addEmployee', component: AddEditEmployee ,data:{edit: false}},
  { path: 'editEmployee', component: AddEditEmployee, data:{edit: true} },
];


export const routing = RouterModule.forRoot(routes);