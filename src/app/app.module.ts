import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule,InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { EmployeesDataService } from './employees/employeesdata.service';
import { EmployeeServiceProvider } from './employees/employeeserviceprovider.component'; 

import { routing } from './app.routing';
import { AppComponent } from './app.component';

import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetails } from './employees/empdetails.component';
import { EmployeeFilterPipe } from './employees/employees.pipe';
import { AddEditEmployee } from './employees/addeditemployee.component';

// export function getBaseUrl(): string {
//   return document.getElementsByTagName('base')[0].href;
// }

@NgModule({
  declarations: [AppComponent,EmployeesComponent,AddEditEmployee,EmployeeDetails,EmployeeFilterPipe],
  imports: [BrowserModule,FormsModule,routing,HttpClientModule,HttpClientInMemoryWebApiModule.forRoot(EmployeesDataService)],
  providers: [EmployeeServiceProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
