import { Component,OnInit } from '@angular/core';
import { EmployeeServiceProvider } from './employeeserviceprovider.component';

export interface person {
    id: number,
    name: string,
    location: string,
    email: string,
    mobile: string
};

@Component({
    templateUrl: 'employees.component.html',
    styleUrls: ['employees.component.css']
})
export class EmployeesComponent {
    employeeslist: person[];
    nameFilter: string = '';
    employeeCount: number = null;

    constructor(private _employeeServiceProvider:EmployeeServiceProvider) { };

    ngOnInit() {
        this.getAllEmployees();
    }

    getAllEmployees(){
        this._employeeServiceProvider.getEmployees().subscribe((object) => {this.employeeslist = object.sort((n1,n2) => {
            if (n1.id > n2.id) {
                return 1;
            }
            if (n1.id < n2.id) {
                return -1;
            }
            return 0;
        });
        this.employeeCount = object.length;
        }
        );
    }

    selEmp(emp: any){
        this._employeeServiceProvider.selectEmployee(emp);
    }

    DelEmp(id: number){
        this._employeeServiceProvider.deleteEmployee(id).subscribe();
        this.getAllEmployees();
    }

}