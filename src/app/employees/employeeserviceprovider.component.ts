import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
 
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { person } from './employees.component';
import { EmployeeService } from './employees.service';
 
const cudOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'})};
 
@Injectable()
 
export class EmployeeServiceProvider extends EmployeeService {
  
  sel_emp:any = null;
   
  constructor(private http: HttpClient) {
    super();
   }
 
  getEmployees(): Observable<person[]> {
    return this.http.get<person[]>(this.employeesUrl).pipe(
      catchError(this.handleError)
    );
  }
 
  // get by id - will 404 when id not found
  getEmployee(id: number): Observable<person> {
    const url = `${this.employeesUrl}/${id}`;
    return this.http.get<person>(url).pipe(
      catchError(this.handleError)
    );
  }
 
  addEmployee(person: person): Observable<person> { 
    return this.http.post<person>(this.employeesUrl, person, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  deleteEmployee(person: number | person): Observable<person> {
    const id = typeof person === 'number' ? person : person.id;
    const url = `${this.employeesUrl}/${id}`;
 
    return this.http.delete<person>(url, cudOptions).pipe(
      catchError(this.handleError)
    );
  }
 
  searchEmployee(term: string): Observable<person[]> {
    term = term.trim();
    // add safe, encoded search parameter if term is present
    const options = term ?
    { params: new HttpParams().set('name', term)} : {};
 
    return this.http.get<person[]>(this.employeesUrl, options).pipe(
      catchError(this.handleError)
    );
  }
 
  updateEmployee(person: person): Observable<person> {
    return this.http.put<person>(this.employeesUrl, person, cudOptions).pipe(
      catchError(this.handleError)
    );
  }

  selectEmployee(emp){
    this.sel_emp = emp;
  }

  getSelectedEmployee(){
    return this.sel_emp;
  }
   
  private handleError(error: any) {
    console.error(error);
    return throwError(error);    
  }
 
}