import { InMemoryDbService } from 'angular-in-memory-web-api'
import { Injectable } from '@angular/core';
import { person } from './employees.component';

@Injectable({
    providedIn: 'root',
})
export class EmployeesDataService implements InMemoryDbService{

    sel_emp: any = null;

    constructor() { }
    createDb(){
    let employees: person[] = [
        {
            id: 2,
            name: "Raj",
            location: "Chennai",
            email: "raj@mail.com",
            mobile: "7867534521"
        },
        {
            id: 1,
            name: "Ram",
            location: "Bangalore",
            email: "ram@mail.com",
            mobile: "9867512345"
          },
          {
            id: 3,
            name: "Vinay",
            location: "Pune",
            email: "vinay@mail.com",
            mobile: "9975287450"
          }
  
    ];
    return {employees};
  }

  genId(employees: person[]): number{
    return employees.length> 0 ? Math.max(...employees.map(emp => emp.id)) + 1 : 1;
  }
}