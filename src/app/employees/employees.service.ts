import { person } from './employees.component';
import { Observable } from 'rxjs';

export abstract class EmployeeService {
    employeesUrl = 'api/employees';
   
    abstract getEmployees(): Observable<person[]>;
    abstract getEmployee(id: number): Observable<person>;
    abstract addEmployee(person: person): Observable<person>;
    abstract deleteEmployee(person: person | number): Observable<person>;
    abstract searchEmployee(term: string): Observable<person[]>;
    abstract updateEmployee(person: person): Observable<person>;
   
  }