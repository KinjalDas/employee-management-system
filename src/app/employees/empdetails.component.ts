import { Component,OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { EmployeeServiceProvider } from './employeeserviceprovider.component';

@Component({
    templateUrl: 'empdetails.component.html',
    styleUrls: ['empdetails.component.css']
})
export class EmployeeDetails{
    employee: any = null;

    constructor(private _employeesServiceProvider:EmployeeServiceProvider,private router: Router) { };

    ngOnInit() {
        this.employee = this._employeesServiceProvider.getSelectedEmployee();
        if(this.employee === null){
            this.router.navigate(["/employees"]);
        }
    }

    goBack(){
        this.router.navigate(["/employees"]);
    }

}