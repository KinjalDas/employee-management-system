import { Component,OnInit } from '@angular/core';
import { EmployeeServiceProvider } from './employeeserviceprovider.component';
import { NgForm } from '@angular/forms';
import {Router} from "@angular/router";
import { ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'addeditemployee.component.html',
    styleUrls: ['addeditemployee.component.css']
})
export class AddEditEmployee{
    cities: string[] = ["Bangalore","Chennai","Pune","Hyderabad"];
    title: string = "Add Employee";
    name: string = "";
    location: string = "";
    email: string = "";
    mobile: string = "";
    id: string = "";
    employee: any = null;
    edit: boolean = false;
    submitbuttonid: string = "add";

    constructor(private _employeeServiceProvider:EmployeeServiceProvider, private router: Router, private activatedroute: ActivatedRoute) { };
    
    ngOnInit(){
        this.activatedroute.data.subscribe(data => {
            this.edit=data.edit;
        });
        if(this.edit===true){
        this.employee = this._employeeServiceProvider.getSelectedEmployee();
        if(this.employee!==null){
            this.title = "Edit Employee";
            this.submitbuttonid = "update";
            this.id = this.employee.id;
            this.name = this.employee.name;
            this.location = this.employee.location;
            this.email = this.employee.email;
            this.mobile = this.employee.mobile;
        }
        else{
            this.router.navigate(['/employees']);
        }
        }

    }

    OnSubmit(form: NgForm){
        if(form.valid){
            if(this.edit){
                let value = form.value;
                value["id"] = this.id;
                this._employeeServiceProvider.updateEmployee(form.value).subscribe();
            }else{
                this._employeeServiceProvider.addEmployee(form.value).subscribe();
            }
            this.router.navigate(['/employees']);
        }
    }
}